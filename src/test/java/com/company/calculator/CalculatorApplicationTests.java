package com.company.calculator;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.company.calculator.controllers.CalculatorController;
import com.company.calculator.services.impl.CalculatorService;

@SpringBootTest
class CalculatorApplicationTests {

	@Autowired
	private CalculatorController calculatorController;
	@Autowired
	private CalculatorService calculatorService;
	
	@Test
	void contextLoads() {
		assertThat(calculatorController).isNotNull();
		assertThat(calculatorService).isNotNull();
	}

}
