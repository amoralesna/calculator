package com.company.calculator.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.company.calculator.services.impl.CalculatorService;

@SpringBootTest
@AutoConfigureMockMvc
public class CalculatorControllerTests {

	private String RESOURCE_ADDITION = "/api/v1/calculator/addition/{addend}/{augend}";
	private String RESOURCE_SUBSTRACTION = "/api/v1/calculator/substraction/{minuent}/{subtrahend}";
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CalculatorService service;
	
	@Test
	void givenTwoNumbers_whenAdd_thenReturnAdditon() throws Exception {
		
		when(service.add(new BigDecimal("1"), new BigDecimal("1"))).thenReturn(new BigDecimal("2"));
		
		this.mockMvc
			.perform(get(RESOURCE_ADDITION, "1", "1"))
			.andExpect(status().isOk())
			.andExpect(content().string(containsString("{\"result\":\"2\"}")));
	}
	
	@Test
	void givenTwoNumbersNegatives_whenAdd_thenReturnAdditon() throws Exception {
		
		when(service.add(new BigDecimal("-3"), new BigDecimal("-2"))).thenReturn(new BigDecimal("-5"));
		
		this.mockMvc
			.perform(get(RESOURCE_ADDITION, "-3", "-2"))
			.andExpect(status().isOk())
			.andExpect(content().string(containsString("{\"result\":\"-5\"}")));
	}
	
	@Test
	void givenTwoNumbers_whenSubstract_thenReturnSubstraction() throws Exception {
		
		when(service.substract(new BigDecimal("5"), new BigDecimal("2"))).thenReturn(new BigDecimal("3"));
		
		this.mockMvc
			.perform(get(RESOURCE_SUBSTRACTION, "5", "2"))
			.andExpect(status().isOk())
			.andExpect(content().string(containsString("{\"result\":\"3\"}")));
	}
	
	@Test
	void givenOneNumber_whenAdd_thenReturnNotFound() throws Exception {

		when(service.substract(new BigDecimal("5"), new BigDecimal("2"))).thenReturn(new BigDecimal("3"));
		
		this.mockMvc
			.perform(get(RESOURCE_ADDITION, "1", null))
			.andExpect(status().isNotFound());
			
	}
	
	@Test
	void givenOneNumberAndLetter_whenSubstraction_thenBadRequest() throws Exception {
		
		when(service.substract(new BigDecimal("5"), new BigDecimal("2"))).thenReturn(new BigDecimal("3"));
		
		this.mockMvc
			.perform(get(RESOURCE_SUBSTRACTION, "1", "a"))
			.andExpect(status().isBadRequest());
	}
	
}
