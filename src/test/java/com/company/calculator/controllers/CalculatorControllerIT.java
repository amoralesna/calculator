package com.company.calculator.controllers;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.company.calculator.dtos.CalculatorDto;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CalculatorControllerIT {
	
	private String RESOURCE_ADDITION = "/api/v1/calculator/addition/{addend}/{augend}";
	private String RESOURCE_SUBSTRACTION = "/api/v1/calculator/substraction/{minuent}/{subtrahend}";
	private String HTTP_HOST = "http://localhost:";
	
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	void givenTwoNumbers_whenAdd_thenReturnAdditon() throws Exception {
		ResponseEntity<CalculatorDto> response = this.restTemplate.getForEntity(HTTP_HOST + port + RESOURCE_ADDITION, CalculatorDto.class, "1", "2");
		assertTrue(response.getBody().getResult().equals("3"));
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
		
	}
	
	@Test
	void givenTwoNumbers_whenSubstract_thenReturnSubstraction() throws Exception {
		ResponseEntity<CalculatorDto> response = this.restTemplate.getForEntity(HTTP_HOST + port + RESOURCE_SUBSTRACTION, CalculatorDto.class, "3", "1");
		assertTrue(response.getBody().getResult().equals("2"));
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
		
	}
}
