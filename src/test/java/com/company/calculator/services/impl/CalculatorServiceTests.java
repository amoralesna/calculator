package com.company.calculator.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.company.calculator.exceptions.ParamNotFoundException;

@SpringBootTest
public class CalculatorServiceTests {

	
	@Autowired
	private CalculatorService calculatorService;
	
	@Test
	void testAddOk() throws Exception {
		
		assertEquals(new BigDecimal("10"), calculatorService.add(new BigDecimal("5"), new BigDecimal("5")));
	}
	
	@Test
	void testAddNegativeNumbersOk() throws Exception {
		
		assertEquals(new BigDecimal("-4"), calculatorService.add(new BigDecimal("-3"), new BigDecimal("-1")));
	}
	
	@Test
	void testSubstractOk() throws Exception {
		
		assertEquals(new BigDecimal("5"), calculatorService.substract(new BigDecimal("10"), new BigDecimal("5")));
	}
	
	@Test
	void testSubstractNegativeNumbersOk() throws Exception {
		
		assertEquals(new BigDecimal("-3"), calculatorService.substract(new BigDecimal("-5"), new BigDecimal("-2")));
	}
	
	@Test
	void testAddNumberAndNullFail() {
		assertThrows(ParamNotFoundException.class, () -> {
			calculatorService.add(new BigDecimal("10"), null);
	    });

	}
	
	@Test 
	void testAddNullAndNumberFail() {
		assertThrows(ParamNotFoundException.class, () -> {
			calculatorService.add(null, new BigDecimal("10"));
	    });

	}
	
	@Test
	void testSubstractNumberAndNullFail() {
		assertThrows(ParamNotFoundException.class, () -> {
			calculatorService.substract(new BigDecimal("10"), null);
	    });

	}
	
	@Test 
	void testSubstractNullAndNumberFail() {
		assertThrows(ParamNotFoundException.class, () -> {
			calculatorService.substract(null, new BigDecimal("10"));
	    });

	}
}
