package com.company.calculator.controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.calculator.dtos.CalculatorDto;
import com.company.calculator.services.ICalculatorService;

import io.corp.calculator.TracerImpl;



@RestController
@RequestMapping("/api/v1/calculator")
public class CalculatorController {

	private final ICalculatorService calculatorservice;
	private final TracerImpl tracer;
	
	@Autowired
	public CalculatorController(ICalculatorService calculatorservice, TracerImpl tracer) {
		this.calculatorservice = calculatorservice;
		this.tracer = tracer;
	}
	
	@GetMapping("/addition/{addend}/{augend}")
	public ResponseEntity<CalculatorDto> addition(@PathVariable("addend") BigDecimal addend,
			@PathVariable("augend") BigDecimal augend) throws Exception {
		
		tracer.trace("Addition request received :: input params :: " + addend + ", " + augend);
		CalculatorDto result = CalculatorDto.builder().result(calculatorservice.add(addend, augend).toString()).build();

		return new ResponseEntity<CalculatorDto>(result, HttpStatus.OK);
	}

	@GetMapping("/substraction/{minuent}/{subtrahend}")
	public ResponseEntity<CalculatorDto> substraction(@PathVariable("minuent") BigDecimal minuent,
			@PathVariable("subtrahend") BigDecimal subtrahend) throws Exception {

		tracer.trace("substraction request received :: input params :: " + minuent + ", " + subtrahend);
		CalculatorDto result = CalculatorDto.builder().result(calculatorservice.substract(minuent, subtrahend).toString()).build();

		return new ResponseEntity<CalculatorDto>(result, HttpStatus.OK);
	}
}
