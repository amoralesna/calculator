package com.company.calculator.services.impl;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.company.calculator.exceptions.ParamNotFoundException;
import com.company.calculator.services.ICalculatorService;

import io.corp.calculator.TracerImpl;

@Service
public class CalculatorService implements ICalculatorService {
	
	private final TracerImpl tracer;
	
	public CalculatorService(TracerImpl tracer) {
		this.tracer = tracer;
	}
	
	@Override
	public BigDecimal add(BigDecimal addend, BigDecimal augend) throws Exception {
		BigDecimal result = null;

		result = Optional.ofNullable(addend).orElseThrow(() -> new ParamNotFoundException("addend is null"))
				.add(Optional.ofNullable(augend).orElseThrow(() -> new ParamNotFoundException("augend is null")));
		tracer.trace("@add service :: " + result);

		return result;
	}

	@Override
	public BigDecimal substract(BigDecimal minuent, BigDecimal subtrahend) throws Exception {
		
		BigDecimal result = Optional.ofNullable(minuent).orElseThrow(() -> new ParamNotFoundException("minuent is null"))
				.subtract(Optional.ofNullable(subtrahend).orElseThrow(() -> new ParamNotFoundException("subtrahend is null")));
		tracer.trace("@substract service :: " + result);
		return result;
	}

}
