package com.company.calculator.services;

import java.math.BigDecimal;

public interface ICalculatorService {
	
	public BigDecimal add (BigDecimal addend, BigDecimal augend) throws Exception;
	public BigDecimal substract (BigDecimal minuent, BigDecimal subtrahend) throws Exception;
	
}
