package com.company.calculator.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.corp.calculator.TracerImpl;

@ControllerAdvice
public class ApiExceptionHandler {
	
	private final TracerImpl tracer;

	@Autowired
	public ApiExceptionHandler(TracerImpl tracer) {
		this.tracer = tracer;
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({ ParamNotFoundException.class })
	@ResponseBody
	public ErrorMessage exception(Exception e) {
		tracer.trace("An error ocurred: " + e.getMessage());
		return new ErrorMessage(e, HttpStatus.INTERNAL_SERVER_ERROR.value());
	}
}
