package com.company.calculator.exceptions;

public class ParamNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private static final String MESSAGE = "Param not found exception";

    public ParamNotFoundException(String detail) {
        super(MESSAGE + " :: " + detail);
    }

}
