package com.company.calculator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.corp.calculator.TracerImpl;

@Configuration
public class LoggerConfig {

	@Bean
	public TracerImpl tracer() {
		return new TracerImpl();
	}
}
