# Calculator microservice

Calculator application implemented as a microservice with Java 11, SpringBoot and Maven that publishes a REST API to perform addition and subtraction of two numbers.

## Instructions

- git clone https://gitlab.com/amoralesna/calculator
- mvn clean (Installs in your maven repository the tracer dependecy)
- mvn package

## How to run

- mvn spring-boot:run

## Endpoints
- *http://localhost:8080/api/v1/calculator/addition/{addend}/{augend}*
- *http://localhost:8080/api/v1/calculator/substraction/{minuent}/{subtrahend}*


## API Documentation
Once the application has started, you can consult the API documentation at:

- *http://localhost:8080/swagger-ui/index.html#/*
